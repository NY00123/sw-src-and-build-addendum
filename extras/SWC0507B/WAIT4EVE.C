//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

waitforeverybody()
    {
    long i, oldtotalclock, numplayersready;
    char playerreadyflag[MAX_SW_PLAYERS]

    if (numplayers < 2)
         return;

    for (i = connecthead; i >= 0; i = connectpoint2[i])
        playerreadyflag[i] = 0;

    if (myconnectindex == connecthead)
        {
        playerreadyflag[connecthead] = 1;

        oldtotalclock = totalclock - 4;
        do
            {
            if (totalclock >= oldtotalclock + 4)
                {
                oldtotalclock = totalclock;
                tempbuf[0] = 5;
                for (i = connecthead; i >= 0; i = connectpoint2[i])
                    if (i != connecthead)
                        sendpacket(i, tempbuf, 1);
                }
            getpackets();

            numplayersready = 0;
            for (i = connecthead; i >= 0; i = connectpoint2[i])
                numplayersready += playerreadyflag[i];

            }
        while (numplayersready < numplayers);
        }
    else
        {
        oldtotalclock = totalclock - 4;
        do
            {
            if (totalclock >= oldtotalclock + 4)
                {
                oldtotalclock = totalclock;
                tempbuf[0] = 5;
                sendpacket(connecthead, tempbuf, 1);
                }
            getpackets();
            }
        while (playerreadyflag[connecthead] == 0);
        }
    }
