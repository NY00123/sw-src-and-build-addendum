//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#define updatecrc(dcrc,xz) dcrc = ((crctable[(dcrc>>8)^xz]^(dcrc<<8))&65535))

initcrc()
{
   long i, j, k, a;

   for(j=0;j<256;j++)      //Calculate CRC table
   {
      k = (j<<8); a = 0;
      for(i=7;i>=0;i--)
      {
         if (((k^a)&0x8000) > 0)
            a = ((a<<1)&65535) ^ 0x1021;   //0x1021 = genpoly
         else
            a = ((a<<1)&65535);
         k = ((k<<1)&65535);
      }
      crctable[j] = (a&65535);
   }
}

getsyncstat()
{
   long i, j, crc;
   spritetype *spr;

   crc = 0;
   updatecrc(crc,randomseed&255);
   updatecrc(crc,(randomseed>>8)&255);
   for(i=connecthead;i>=0;i=connectpoint2[i])
   {
      updatecrc(crc,posx[i]&255);
      updatecrc(crc,posy[i]&255);
      updatecrc(crc,posz[i]&255);
      updatecrc(crc,ang[i]&255);
      updatecrc(crc,horiz[i]&255);
      updatecrc(crc,health[i]&255);
   }

   for(i=7;i>=0;i--)
      for(j=headspritestat[i];j>=0;j=nextspritestat[j])
      {
         spr = &sprite[j];
         updatecrc(crc,(spr->x)&255);
         updatecrc(crc,(spr->y)&255);
         updatecrc(crc,(spr->z)&255);
         updatecrc(crc,(spr->ang)&255);
      }
   return(crc);
}
