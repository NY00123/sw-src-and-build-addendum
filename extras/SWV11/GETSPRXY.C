//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

getspritexyinsector(long sectnum, long *x, long *y)
{
    long x1, y1, x2, y2, i;

    i = sector[sectnum].wallptr;
    x1 = wall[i].x;
    y1 = wall[i].y;
    i = wall[i].point2;
    x2 = wall[i].x;
    y2 = wall[i].y;
    *x = ((x1+x2)>>1) + ksgn(y1-y2);
    *y = ((y1+y2)>>1) + ksgn(x2-x1);
}
