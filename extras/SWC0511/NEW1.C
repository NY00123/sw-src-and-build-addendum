//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

doanimations(long numtics)
    {
    long i, j;

    for (i = animatecnt - 1; i >= 0; i--)
        {
        j = *animateptr[i];

        if (j < animategoal[i])
            j = min(j + animatevel[i] * numtics, animategoal[i]);
        else
            j = max(j - animatevel[i] * numtics, animategoal[i]);

        *animateptr[i] = j;

        if (j == animategoal[i])
            {
            animatecnt--;
            if (i != animatecnt)
                {
                animateptr[i] = animateptr[animatecnt];
                animategoal[i] = animategoal[animatecnt];
                animatevel[i] = animatevel[animatecnt];
                }
            }
        }
    }

getanimationgoal(long animptr)
    {
    long i;

    for (i = animatecnt - 1; i >= 0; i--)
        if (animptr == animateptr[i])
            return (i);
    return (-1);
    }

setanimation(long *animptr, long thegoal, long thevel)
    {
    long i, j;

    if (animatecnt >= MAXANIMATES)
        return (-1);

    j = animatecnt;
    for (i = animatecnt - 1; i >= 0; i--)
        if (animptr == animateptr[i])
            {
            j = i;
            break;
            }

    animateptr[j] = animptr;
    animategoal[j] = thegoal;
    animatevel[j] = thevel;
    if (j == animatecnt)
        animatecnt++;
    return (j);
    }
