//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

// ZWINFONT.C
// Window font routines

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include "build.h"
#include "proto.h"
#include "keys.h"
#include "names2.h"
#include "panel.h"
#include "game.h"
#include "tags.h"
#include "sector.h"
#include "sprite.h"
#include "weapon.h"
#include "player.h"
#include "panel.h"
#include "control.h"
#include "sw_strs.h"
#include "zwin.h"
#include "zwinfont.h"

// GLOBALS ////////////////////////////////////////////////////////////////////////////////////////

int theWidth;
int theCharLength;
ZFontStruct	*fontStruct;
char *theString;

// FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
// Start up Z Windows
////////////////////////////////////////
void Z_LaunchWindows( void )
{
}

////////////////////////////////////////
// Find out the height of a given font.
////////////////////////////////////////
short Z_FontHeight (ZFontStruct_p font)
{
	short theFontHeight;

	theFontHeight = font->ascent + font->descent;

	return (theFontHeight);
}

