//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------


 if(keystatus[0x28]==1 && keystatus[0x2e]==1) // ' C
      {
     keystatus[0x2e] = 0;
         switch (searchstat)
         {
            case 0: case 4:
        for(i=0;i<MAXWALLS;i++)
        {
         if(wall[i].picnum==temppicnum) {wall[i].shade=tempshade;}
        }
                break;
            case 1: case 2:
        for(i=0;i<MAXSECTORS;i++)
        {
        if(searchstat==1)
         if(sector[i].ceilingpicnum==temppicnum) {sector[i].ceilingshade=tempshade;}
        if(searchstat==2)
         if(sector[i].floorpicnum==temppicnum) {sector[i].floorshade=tempshade;}
        }
                break;
            case 3:
        for(i=0;i<MAXSPRITES;i++)
        {
         if(sprite[i].picnum==temppicnum) {sprite[i].shade=tempshade;}
        }
                break;
         }
      }

 if(keystatus[0x28]==1 && keystatus[0x14]==1) // ' T
      {
         keystatus[0x14] = 0;
         switch (searchstat)
         {
            case 0: case 4:
                strcpy(tempbuf,"Wall lotag: ");
                wall[searchwall].lotag =
                getnumber256(tempbuf,wall[searchwall].lotag,65536L);
                break;
            case 1: case 2:
                strcpy(tempbuf,"Sector lotag: ");
                sector[searchsector].lotag =
                getnumber256(tempbuf,sector[searchsector].lotag,65536L);
                break;
            case 3:
                strcpy(tempbuf,"Sprite lotag: ");
                sprite[searchwall].lotag =
                getnumber256(tempbuf,sprite[searchwall].lotag,65536L);
                break;
         }
      }

 if(keystatus[0x28]==1 && keystatus[0x23]==1) // ' H
      {
         keystatus[0x23] = 0;
         switch (searchstat)
         {
            case 0: case 4:
                strcpy(tempbuf,"Wall hitag: ");
                wall[searchwall].hitag =
                getnumber256(tempbuf,wall[searchwall].hitag,65536L);
                break;
            case 1: case 2:
                strcpy(tempbuf,"Sector hitag: ");
                sector[searchsector].hitag =
                getnumber256(tempbuf,sector[searchsector].hitag,65536L);
                break;
            case 3:
                strcpy(tempbuf,"Sprite hitag: ");
                sprite[searchwall].hitag =
                getnumber256(tempbuf,sprite[searchwall].hitag,65536L);
                break;
         }
      }

 if(keystatus[0x28]==1 && keystatus[0x1f]==1) // ' S
      {
     keystatus[0x1f] = 0;
         switch (searchstat)
         {
            case 0: case 4:
        strcpy(tempbuf,"Wall shade: ");
        wall[searchwall].shade =
        getnumber256(tempbuf,wall[searchwall].shade,65536L);
                break;
            case 1: case 2:
        strcpy(tempbuf,"Sector shade: ");
        if(searchstat==1)
         sector[searchsector].ceilingshade =
         getnumber256(tempbuf,sector[searchsector].ceilingshade,65536L);
        if(searchstat==2)
         sector[searchsector].floorshade =
         getnumber256(tempbuf,sector[searchsector].floorshade,65536L);
                break;
            case 3:
        strcpy(tempbuf,"Sprite shade: ");
        sprite[searchwall].shade =
        getnumber256(tempbuf,sprite[searchwall].shade,65536L);
                break;
         }
      }

 if(keystatus[0x28]==1 && keystatus[0x2f]==1) // ' V
      {
     keystatus[0x2f] = 0;
         switch (searchstat)
         {
            case 1: case 2:
        strcpy(tempbuf,"Sector visibility: ");
        sector[searchsector].visibility =
         getnumber256(tempbuf,sector[searchsector].visibility,65536L);
                break;
         }
      }
