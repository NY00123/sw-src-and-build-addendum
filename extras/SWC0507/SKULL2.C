//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#include <stdlib.h>
#include "build.h"
#include "proto.h"
#include "keys.h"
#include "names2.h"
#include "panel.h"
#include "game.h"
#include "tags.h"
#include "ai.h"

//////////////////////    
//
// SERP RUN
//
//////////////////////

ANIMATOR DoSerpMove,NullAnimator,DoActorDebris,NullAnimator;
 
int 
SetupSerp(short SpriteNum)    
{
    SPRITEp sp = &sprite[SpriteNum];
    USERp u;
    ANIMATOR DoActorDecide;
    
    if (TEST(sp->extra, SPRX_RESTORE))
        {
        u = User[SpriteNum];
        }
    else
        {
        User[SpriteNum] = u = SpawnUser(SpriteNum,SERP_RUN_R0,&s_SerpRun[0][0]);
        u->Health = 300;
        u->Attrib = &DefaultAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        }

    u->StateStart = u->State;
    u->StateEnd = s_SerpDie;
    u->Rot = sg_SerpRun;

    EnemyDefaults(SpriteNum, &SerpActionSet, &SerpPersonality);
    sp->clipdist = (512 + 256) >> 2;
    SET(u->Flags, SPR_XFLIP_TOGGLE);
    
    u->loz = sp->z;
    
    return(0);
}    


int DoSerpMove(short SpriteNum)
{
    SPRITEp sp = &sprite[SpriteNum];
    USERp u = User[SpriteNum];
    
    if (TEST(u->Flags,SPR_SLIDING))
        DoActorSlide(SpriteNum);

    if (u->track >= 0)
        ActorFollowTrack(SpriteNum, ACTORMOVETICS);
    else
        (*u->ActorActionFunc)(SpriteNum);
    
    KeepActorOnFloor(SpriteNum);
}    


