//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

int
LedgeAhead(short SpriteNum, short min_height)
    {

    SPRITEp sp = &sprite[SpriteNum];
    long dax, day;
    short newsector;

    // dax = sp->x + MOVEx(sp->xvel * synctics, sp->ang);
    // day = sp->y + MOVEy(sp->xvel * synctics, sp->ang);

    dax = sp->x + MOVEx(128, sp->ang);
    day = sp->y + MOVEy(128, sp->ang);

    if (!inside(dax, day, sp->sectnum))
        {

        newsector = sp->sectnum;

        updatesector(dax, day, &newsector);

        #if 0
        if (TEST(sector[newsector].extra, SECTFX_SPECIAL))
            {
            SECT_USERp su = SectUser[newsector];

            ASSERT(su);
            
            switch (su->special)
                {
                case SPECIAL_BLOCK_ACTOR:
                    return (TRUE);
                    break;
                }
            }
        #endif    

        if (labs(sector[newsector].floorz - sector[sp->sectnum].floorz) > min_height)
            {
            return (TRUE);
            }
        }

    return (FALSE);
    }

int
move_actor(short SpriteNum, long xchange, long ychange, long zchange)
    {
    USERp u = User[SpriteNum];
    SPRITEp sp = User[SpriteNum]->SpriteP;

    long x, y, z;
    short sp_sect, orig_sect, dist, LedgeHeight;

    short ret;

    // Look out in front for some sort of ledge

    if (!TEST(u->Flags, SPR_JUMPING | SPR_FALLING))
        {
        if (TEST(u->Flags, SPR_DEATH))
            LedgeHeight = 0;
        else
            LedgeHeight = Z(20);


        if (LedgeAhead(SpriteNum, LedgeHeight))
            {
            return (FALSE);
            }
        }

    // save off x,y values
    x = sp->x;
    y = sp->y;

    // move z up off of the floor
    z = sp->z - Z(16);

    orig_sect = sp_sect = sp->sectnum;

    // scale by synctics
    xchange *= synctics;
    ychange *= synctics;
    zchange *= synctics;

    // some shifting value Ken put in to maintain compatibility
    xchange <<= 11;
    ychange <<= 11;

    ret = clipmove(&x, &y, &z, &sp_sect, xchange, ychange, walldist, u->ceiling_dist, u->floor_dist, 0);
//    ret = clipmove(&x, &y, &z, &sp_sect, xchange, ychange, sp->clipdist<<2, u->ceiling_dist, u->floor_dist, 0);

    if (!ret)
        {
        // Keep track of how far sprite has moved
        dist = Distance(x, y, sp->x, sp->y);
        u->Dist += dist;
        u->ZigZagDist += dist;

        // Place sprite in new position
        setsprite(SpriteNum, x, y, sp->z);

        return (TRUE);
        }
    else
        {
        return (FALSE);
        }

    }

