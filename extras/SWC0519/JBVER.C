//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#define COINCURSOR 2440

int intro;
void ExtCheckKeys(void)
{
 if (qsetmode == 200)    //In 3D mode
 {
  if(intro<600)
  {
   intro++;
   sprintf(tempbuf,"SWBUILD Level Editor : VER: 1.0  10-30-96");
   printext256(48*8,(24*8)-1,0,-1,tempbuf,1);
   printext256(48*8,24*8,WHITE,-1,tempbuf,1);
   rotatesprite((320-8)<<16,(200-8)<<16,64<<9,0,COINCURSOR+(((4-totalclock>>3))&7),0,0,0,0,0,xdim-1,ydim-1);
  }
 }
}// end
