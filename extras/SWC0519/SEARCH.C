//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

short sectlist[MAXSECTORS], sectlistcnt, sectlistend;

sectlist[0] = endsect;
sectlistcnt = 0; sectlistend = 1;
while (sectlistcnt < sectlistend)
{
      //Read off next sector on list to process
   dasect = sectlist[sectlistcnt++];

   //process dasect
   //for(i=headspritesect[dasect]...
   
      //Scan all red walls of dasect
   startwall = sector[dasect].wallptr;
   endwall = startwall + sector[dasect].wallnum;
   for(w=startwall;w<endwall;w++)
   {
      nextsector = wall[w].nextsector;
      if (nextsector < 0) continue;

         //Check if new sector is already on sectlist
      for(s=sectlistend-1;s>=0;s--)
         if (sectlist[s] == nextsector) break;

      if (s < 0)  //Did not break out of loop, so no matches were found
         sectlist[sectlistend++] = nextsector;
   }
}
