//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

SoundShutdown();
MusicShutdown();
gs.SoundVolume = FX_GetVolume();
FX_SetCallBack(SoundCallBack);
MUSIC_PlaySong(SongPtr, loopflag);
FX_StopAllSounds();
MUSIC_StopSong();
voice = FX_PlayLoopedVOC(vp->data, start, 65536, pitch, 255 - sound_dist, 255 - sound_dist, 255 - sound_dist, priority, num);
voice = FX_PlayVOC3D(vp->data, pitch, angle, sound_dist, priority, num);
FX_SetReverseStereo(!gs.FlipStereo);
status = FX_Init(FXDevice, NumVoices, NumChannels, NumBits, MixRate);
FX_SetVolume(FXVolume);
status = FX_SetCallBack(SoundCallBack);
Error(FX_ErrorString(FX_Error));
status = FX_Shutdown();
status = MUSIC_Init(MusicDevice, MidiPort);
MUSIC_SetVolume(MusicVolume);
status = MUSIC_Shutdown();
FX_SoundActive(vp->handle)
FX_StopSound(vp->handle);       
FX_SoundsPlaying();
FX_Pan3D(p->handle, angle, dist);
FX_SetPitch(p->handle, pitch);

