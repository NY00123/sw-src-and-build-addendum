//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

// TESTDLL.C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "exports.h"

XREF XR;

void __export
SetupExports (XREFp ExportList)
    {
    XR.TestMe = ExportList->TestMe;    
    XR.TestMe2 = ExportList->TestMe2;    
    }

void __export
TestIt( void )
    {
    XR.TestMe();
    XR.TestMe2(10);
    }

    