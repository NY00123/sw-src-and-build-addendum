//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

void ExtLoadMap(char *mapname)
{

/********************/

 qloadkvx(0L,"medkit2.kvx");
 qloadkvx(1L,"tank.kvx"); /1975 TANK

/********************/

}

void ExtAnalyzeSprites(void)
{

/************************/

    switch(tspr->picnum)
        {
         case 1803 : tspr->picnum=0; tspr->cstat|=48; break;
         case 1975 : tspr->picnum=1; tspr->cstat|=48; break;
        }

/**************************/

}
