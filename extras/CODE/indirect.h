//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#ifndef INDIRECT_H

#define INDIRECT_H

// INDIRECT.H
// Defines a structure which contains pointers to all functions external 
// to the DLL, but which the DLL needs access to in another module.
// Copyright(c)1997 Jim Norwood

struct INDIRECTstruct;
typedef struct INDIRECTstruct XFER, *XFERp;
    
struct INDIRECTstruct
    {
    int (*MyTestFunc)(short SpriteNum);
    };

    
#endif