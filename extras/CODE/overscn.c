//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#include <i86.h>

union  REGS  regs;
struct SREGS sregs;

/* Palette Color Codes
bit #   Color EGA Monitor   Color RGBI Monitor
==============================================
5       Secondary Red         ----
4       Secondary Green       Intensity
3       Secondary Blue        ----
2       Red                   Red
1       Green                 Green
0       Blue                  Blue
*/

// Set code to 0 for black
void SetOverScan(char code)
{
    regs.h.ah = 10;
    regs.h.al = 1
    regs.h.bh = code;

    #ifdef __386__
        int386( 0x10, &regs, &regs );
    #else
        int86( 0x10, &regs, &regs );
    #endif
}