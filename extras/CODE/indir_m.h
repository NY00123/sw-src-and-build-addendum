//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------


#include "indirect.h"

XREF IndirectList = 
    {
    MyTestFunc
    };
    
    