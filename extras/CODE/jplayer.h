//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

#ifndef JPLAYER_H
#define JPLAYER_H

// BOT DEFINITIONS AND STRUCTURES

typedef struct BOT_BRAIN
    {
    short tgtinv;
    short tgtitem;
    short tgtenemy;
    
    } BotBrain, *BotBrain_p;


#endif