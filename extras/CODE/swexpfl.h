//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

// SWEXPFL.H
// This is a list of every function that is exported from SW for DLL use
// These prototypes are devoid of any typedef identifiers since Frank's
// BOOL, VOID, etc. declarations are identical to those in a Dos4G header. DOH!
// 

#ifndef SWEXPFL_H
#define SWEXPFL_H

char ValidPtr(void *ptr);
void * AllocMem(int size);
void * CallocMem(int size, int num);
void * ReAllocMem(void *ptr, int size);
void FreeMem(void *ptr);

#endif
