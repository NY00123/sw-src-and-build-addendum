//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is NOT part of Shadow Warrior (1997).
However, it is either an older version of a file that is, or is some
test code written or used during the development of Shadow Warrior (1997).
This file is provided purely for educational interest.

*/
//-------------------------------------------------------------------------

// cmdlib.h

#ifndef __CMDLIB__
#define __CMDLIB__

#ifdef __NeXT__
#define stricmp strcasecmp
#endif

typedef unsigned char byte;
typedef enum {false,true} boolean;


void    Error (char *error, ...);
int     CheckParm (char *check);

int     SafeOpenWrite (char *filename);
int     SafeOpenRead (char *filename);
void    SafeRead (int handle, void *buffer, long count);
void    SafeWrite (int handle, void *buffer, long count);
void    *SafeMalloc (long size);

long    LoadFile (char *filename, void **bufferptr);
void    SaveFile (char *filename, void *buffer, long count);

void    DefaultExtension (char *path, char *extension);
void    DefaultPath (char *path, char *basepath);
void    StripFilename (char *path);
void    ExtractFileBase (char *path, char *dest);

long    ParseNum (char *str);

short   BigShort (short l);
short   LittleShort (short l);
long    BigLong (long l);
long    LittleLong (long l);

void    GetPalette (byte *pal);
void    SetPalette (byte *pal);
void    VGAMode (void);
void    TextMode (void);

#endif
