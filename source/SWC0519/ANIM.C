//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is part of Shadow Warrior (1997).

Shadow Warrior (1997) is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

*/
//-------------------------------------------------------------------------

#include <i86.h>
#include "build.h"
#include "proto.h"
#include "keys.h"
#include "types.h"
#include "develop.h"
#include "sndcards.h"
#include "fx_man.h"
#include "music.h"
#include "scriplib.h"
#include "file_lib.h"
#undef MAXPLAYERS
#include "gamedefs.h"
#include "keyboard.h"
#include "util_lib.h"

#include "control.h"
#include "config.h"
#include "sounds.h"
#include "function.h"

#include "def.h"

#include "animlib2.h"

#define MAX_ANMS 10
anim_t *anm_ptr[MAX_ANMS];

long ANIMnumframes;
char ANIMpal[3*256];
char ANIMnum = 0;

char *ANIMname[] = 
{
"sw.anm",
"swend.anm"
};

#define ANIM_TILE(num) (MAXTILES-11 + (num))

VOID AnimShareIntro(int frame, int numframes)
    {
    long zero=0;
        
    if (frame == numframes-1)
        ototalclock += 120;
    else    
    if (frame == 1)
        {
        PlaySound(DIGI_NOMESSWITHWANG,&zero,&zero,&zero,v3df_none);
        ototalclock += 120*3;
        }
    else    
        ototalclock += 8;
    
    if (frame == 5)    
        {
        PlaySound(DIGI_INTRO_SLASH,&zero,&zero,&zero,v3df_none);
        }
    else    
    if (frame == 15)    
        {
        PlaySound(DIGI_INTRO_WHIRL,&zero,&zero,&zero,v3df_none);
        }
    }

#if 0    
DIGI_ENTRY("JGSB4.VOC",    DIGI_SERPTAUNTWANG,    497,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("JGB166.VOC",   DIGI_WANGTAUNTSERP1,   498,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("JGB156.VOC",   DIGI_WANGTAUNTSERP2,   499,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("JGB193.VOC",   DIGI_WANGORDER1,       500,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("JGB202.VOC",   DIGI_WANGORDER2,       501,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("JGB340A.VOC",  DIGI_WANGDROWNING,     502,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             

DIGI_ENTRY("JGEN06.VOC",   DIGI_ZILLAREGARDS,     503,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("MSG9.VOC",     DIGI_PMESSAGE,         504,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             

DIGI_ENTRY("UGLY1A.VOC",   DIGI_SHAREND_UGLY1,    505,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("UGLY1B.VOC",   DIGI_SHAREND_UGLY2,    506,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
DIGI_ENTRY("TELEPT07.VOC", DIGI_SHAREND_TELEPORT, 507,      PRI_PLAYERVOICE, 0,      0, NULL, DIST_NORMAL, VF_NORMAL )             
#endif

short SoundState;
VOID AnimShareEnd(int frame, int numframes)
    {
    long zero=0;
    ototalclock += 16;
    
    if (frame == numframes-1)
        ototalclock += 1*120;

    if (frame == 1)
        {
        PlaySound(DIGI_SERPTAUNTWANG,&zero,&zero,&zero,v3df_none);
        }
    else    
    if (frame == 16)
        {
        PlaySound(DIGI_SHAREND_TELEPORT,&zero,&zero,&zero,v3df_none);
        }
    else    
    if (frame == 35)
        {
        SoundState++;
        PlaySound(DIGI_WANGTAUNTSERP1,&zero,&zero,&zero,v3df_none);
        }
    else    
    if (frame == 51)
        {
        SoundState++;
        PlaySound(DIGI_SHAREND_UGLY1,&zero,&zero,&zero,v3df_none);
        }
    else    
    if (frame == 64)
        {
        SoundState++;
        PlaySound(DIGI_SHAREND_UGLY2,&zero,&zero,&zero,v3df_none);
        }
    }
    
char * LoadAnm(short anim_num)
    {
    long handle;
    long length;
    char *animbuf, *palptr;
    long i,j,k;
    
    // this seperate allows the anim to be precached easily
    
    ANIMnum = anim_num;
    
    // lock it
    walock[ANIM_TILE(ANIMnum)] = 219;

    if (anm_ptr[anim_num] == 0)
        {
        handle = kopen4load(ANIMname[ANIMnum], 0);
        if (handle == -1)
            return(NULL);
        length = kfilelength(handle);
        
        allocache((long *) &anm_ptr[anim_num], length + sizeof(anim_t), &walock[ANIM_TILE(ANIMnum)]);
        animbuf = (char *) (FP_OFF(anm_ptr[anim_num]) + sizeof(anim_t));
        
        kread(handle, animbuf, length);
        kclose(handle);
        }
    else    
        {
        animbuf = (char *) (FP_OFF(anm_ptr[anim_num]) + sizeof(anim_t));
        }
    
    return(animbuf);
    }    

void 
playanm(short anim_num)
    {
    char *animbuf, *palptr;
    long i, j, k, length = 0, numframes = 0;
    int32 handle = -1;
    char ANIMvesapal[4*256];
    char backup_pal[3*256];
    char tempbuf[256];
    char *palook_bak = palookup[0];
    
    ANIMnum = anim_num;

    KB_FlushKeyboardQueue();    
    KB_ClearKeysDown();    
    
    animbuf = LoadAnm(anim_num);
    if (!animbuf)    
        return;
    
    for (i = 0; i < 256; i++)
        tempbuf[i] = i;
    palookup[0] = tempbuf;    
    GetPaletteFromVESA(backup_pal);
        
    ANIM_LoadAnim(animbuf);
    ANIMnumframes = ANIM_NumFrames();
    numframes = ANIMnumframes;
    
    palptr = ANIM_GetPalette();
    for (i = 0; i < 256; i++)
        {
        j = (i << 2);
        k = j - i;
        ANIMvesapal[j + 0] = (palptr[k + 2] >> 2);
        ANIMvesapal[j + 1] = (palptr[k + 1] >> 2);
        ANIMvesapal[j + 2] = (palptr[k + 0] >> 2);
        ANIMvesapal[j + 3] = 0;
        }

    tilesizx[ANIM_TILE(ANIMnum)] = 200;
    tilesizy[ANIM_TILE(ANIMnum)] = 320;

    if (ANIMnum == 1)
        {
        // draw the first frame
        waloff[ANIM_TILE(ANIMnum)] = FP_OFF(ANIM_DrawFrame(1));
        rotatesprite(0 << 16, 0 << 16, 65536L, 512, ANIM_TILE(ANIMnum), 0, 0, 2 + 4 + 8 + 16 + 64, 0, 0, xdim - 1, ydim - 1);
        nextpage();
        }
    VBE_setPalette(0L, 256L, ANIMvesapal);

    SoundState = 0;
    //ototalclock = totalclock + 120*2;
    ototalclock = totalclock;

    for (i = 1; i < numframes; i++)
        {
        while (totalclock < ototalclock)
            {
            switch (ANIMnum)
                {
                case 0:
                    if (KB_KeyWaiting())
                        goto ENDOFANIMLOOP;
                    break;
                case 1:
                    if (KEY_PRESSED(KEYSC_ESC))
                        goto ENDOFANIMLOOP;
                    break;
                }    
                
            getpackets();
            }

        switch (ANIMnum)
            {
            case 0:
                AnimShareIntro(i,numframes);    
                break;
            case 1:
                AnimShareEnd(i,numframes);    
                break;
            }    
            
        waloff[ANIM_TILE(ANIMnum)] = FP_OFF(ANIM_DrawFrame(i));
        rotatesprite(0 << 16, 0 << 16, 65536L, 512, ANIM_TILE(ANIMnum), 0, 0, 2 + 4 + 8 + 16 + 64, 0, 0, xdim - 1, ydim - 1);
        nextpage();
        }

    // pause on final frame    
    while (totalclock < ototalclock)
        getpackets();
        
ENDOFANIMLOOP:

    //GetPaletteFromVESA(palette_data);
    //FadeOut(0,5);
    //memcpy(palette_data, backup_pal, 3*256);
    clearview(0);
    nextpage();
    palookup[0] = palook_bak;
    SetPaletteToVESA(backup_pal);
    //GetPaletteFromVESA(palette_data);
    //FadeOut(0,0);
    clearview(0);
    nextpage();
    
    KB_FlushKeyboardQueue();    
    KB_ClearKeysDown();    
    ANIM_FreeAnim();
    walock[ANIM_TILE(ANIMnum)] = 1;
    }
