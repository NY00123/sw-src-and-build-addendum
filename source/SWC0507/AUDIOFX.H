//-------------------------------------------------------------------------
/*
Copyright (C) 2019 GHI Media, LLC

This file is part of Shadow Warrior (1997).

Shadow Warrior (1997) is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

*/
//-------------------------------------------------------------------------

/////////////////////////////////////////////////
//
// MUSE Header for .FX
// Created Mon Jan 10 17:08:25 1994
//
/////////////////////////////////////////////////

#define NUMSOUNDS       2
#define NUMSNDCHUNKS        6

//
// Sound names & indexes
//
typedef enum {
        UNTITLED0SND,            // 0
        UNTITLED1SND,            // 1
        LASTSOUND
         } soundnames;

//
// Base offsets
//
#define STARTPCSOUNDS       0
#define STARTADLIBSOUNDS    2
#define STARTDIGISOUNDS     4
#define STARTMUSIC      6


/////////////////////////////////////////////////
//
// Thanks for playing with MUSE!
//
/////////////////////////////////////////////////
